from django.apps import AppConfig


class DjangocmsPaiBootstrap4Config(AppConfig):
    name = 'djangocms_pai_bootstrap4'
